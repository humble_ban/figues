﻿using System;
using System.Collections.Generic;
using System.Linq;
using FigureCalc;
using FigureCalc.Interfaces;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Trianlge()
        {
            var tr1 = new Triangle(3, 4, 5);
            var tr1square = Operations.GetFigureSquare(tr1);

            Assert.True(tr1.IsRightTriangle());
            Assert.True(tr1square == 6.0);

            var tr2 = new Triangle(1, 2, 3);
            var tr2square = Operations.GetFigureSquare(tr2);
            Assert.True(tr2square == 0.0);
        }

        [Test]
        public void Circle()
        {
            var circ = new Circle(1);
            Assert.True(circ.CalcSquare() == Math.PI);
        }

        [Test]
        public void Array()
        {
            List<double> Squeares = new List<double>();
            IFigure[] arr = new IFigure[3] {new Triangle(3, 4, 5), new Circle(1), new Triangle(1, 2, 3)};

            foreach (var item in arr)
            {
                Squeares.Add(Operations.GetFigureSquare(item));
            }
            Assert.True(Squeares.Any());
        }
    }
}