﻿using System;
using FigureCalc.Interfaces;

namespace FigureCalc
{
    public static class Operations
    {
        public static double GetFigureSquare(IFigure figure)
        {
            return figure.CalcSquare();
        }
    }
}