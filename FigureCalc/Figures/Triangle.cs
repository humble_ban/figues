using System;
using FigureCalc.Interfaces;

namespace FigureCalc
{
    public class Triangle : IFigure
    {
        /// <summary>
        /// Треугольник
        /// </summary>
        /// <param name="a">Длинна стороны a</param>
        /// <param name="b">Длинна стороны b</param>
        /// <param name="c">Длинна стороны c</param>
        private double a, b, c;

        public Triangle(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public bool IsRightTriangle()
        {
            //если гипотенуза сторона а
            if (a * a == b * b + c * c)
            {
                return true;
            }

            //если гипотенуза сторона b
            if (b * b == a * a + c * c)
            {
                return true;
            }

            //если гипотенуза сторона c
            if (c * c == a * a + b * b)
            {
                return true;
            }

            return false;
        }

        public double CalcSquare()
        {
            if (a < 0 || b < 0 || c < 0)
            {
                throw new Exception("Введена сторона с отрицательной длинной, вычисление площали невозможно.");
            }

            double p = (a + b + c) / 2;
            return Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }
    }
}