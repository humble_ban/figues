using System;
using FigureCalc.Interfaces;

namespace FigureCalc
{
    public class Circle : IFigure
    {
        private double radius;

        public Circle(double radius)
        {
            this.radius = radius;
        }

        public double CalcSquare()
        {
            if (this.radius < 0 )
            {
                throw new Exception("Радиус не может быть отрицательным. Вычисление площади невозможно");
            }
            return Math.PI * this.radius * this.radius;
        }
    }
}