namespace FigureCalc.Interfaces
{
    public interface IFigure
    {
        double CalcSquare();
    }
}